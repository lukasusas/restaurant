-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 28, 2016 at 10:02 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Restaurant`
--

-- --------------------------------------------------------

--
-- Table structure for table `Meal`
--

CREATE TABLE `Meal` (
  `id` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Description` text NOT NULL,
  `Photo` text NOT NULL,
  `QuantityInStock` int(11) NOT NULL,
  `BuyPrice` float NOT NULL,
  `SalePrice` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Meal`
--

INSERT INTO `Meal` (`id`, `Name`, `Description`, `Photo`, `QuantityInStock`, `BuyPrice`, `SalePrice`) VALUES
(1, 'Coca-Cola', 'Coca-Cola with 10 pieces of sugar and full of caffeine! Tasty!', 'coca.jpg', 69, 1, 3),
(2, 'Bagel Thon', 'Our bagel is made of a soft bread with sesame seeds and yellowfin tuna, accompanied by fresh daily salad leaves and a stunning sauce', 'bagel_thon.jpg', 15, 3, 6),
(3, 'Bacon Cheeseburger', 'This delicious cheeseburger contains 150g French meat burger and a just right toasted buns, all accompanied by fresh home fries!', 'bacon_cheeseburger.jpg', 13, 6, 13),
(4, 'Carrot Cake', 'The carrot cake house will delight the lovers and purists: all the ingredients are natural! Consume without moderation', 'carrot_cake.jpg', 20, 3, 7),
(5, 'Donut Chocolat', 'The donuts are made that morning and are covered with a delicious chocolate sauce!', 'chocolate_donut.jpg', 16, 3, 6),
(6, 'Dr. Pepper', 'Its sweet taste with almonds will delight you!', 'drpepper.jpg', 0, 1, 3),
(7, 'Milkshake', 'Our creamy milkshake contains pieces of Oreo and is accompanied by whipped cream and smarties as a topping. It will dazzle your taste buds!', 'milkshake.jpg', 19, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `Orderline`
--

CREATE TABLE `Orderline` (
  `id` int(11) NOT NULL,
  `QuantityOrdered` int(11) NOT NULL,
  `MealId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `PriceEach` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Orders`
--

CREATE TABLE `Orders` (
  `id` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `TotalAmount` double NOT NULL,
  `TaxRate` float NOT NULL,
  `TaxAmount` double NOT NULL,
  `CreationTimestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `CompletionTimestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Reservations`
--

CREATE TABLE `Reservations` (
  `id` int(11) NOT NULL,
  `Name` text NOT NULL,
  `NumberOfPersons` int(11) NOT NULL,
  `Date` date NOT NULL,
  `Time` text NOT NULL,
  `PhoneNumber` text NOT NULL,
  `UserId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Surname` text NOT NULL,
  `Email` text NOT NULL,
  `PhoneNumber` text NOT NULL,
  `Password` text NOT NULL,
  `StreetName` text NOT NULL,
  `HouseNumber` text NOT NULL,
  `City` text NOT NULL,
  `Role` int(11) NOT NULL DEFAULT '1',
  `CreationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LastLogin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `Name`, `Surname`, `Email`, `PhoneNumber`, `Password`, `StreetName`, `HouseNumber`, `City`, `Role`, `CreationDate`, `LastLogin`) VALUES
(1, 'admin', 'admin', 'admin@admin.com', '123456789', '4292bb254cd658efc4e2c26addafcccd', 'admin street', '1', 'admincity', 3, '2016-08-19 07:41:04', '2016-08-28 19:48:55'),
(9, 'Testname', 'testsurnames', 'test@test.com', '12345678', 'a1f93e750785418b41fe061a095c5846', 'test street', '123', 'testcity', 1, '2016-08-18 10:08:54', '2016-08-25 17:57:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Meal`
--
ALTER TABLE `Meal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `Orderline`
--
ALTER TABLE `Orderline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `MealId` (`MealId`),
  ADD KEY `OrderId` (`OrderId`);

--
-- Indexes for table `Orders`
--
ALTER TABLE `Orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `UserId` (`UserId`);

--
-- Indexes for table `Reservations`
--
ALTER TABLE `Reservations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UserId_2` (`UserId`),
  ADD KEY `UserId` (`UserId`),
  ADD KEY `UserId_3` (`UserId`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Meal`
--
ALTER TABLE `Meal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `Orderline`
--
ALTER TABLE `Orderline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Orders`
--
ALTER TABLE `Orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Reservations`
--
ALTER TABLE `Reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Orderline`
--
ALTER TABLE `Orderline`
  ADD CONSTRAINT `Orderline_ibfk_1` FOREIGN KEY (`OrderId`) REFERENCES `Orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Orderline_ibfk_2` FOREIGN KEY (`MealId`) REFERENCES `Meal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Orders`
--
ALTER TABLE `Orders`
  ADD CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Reservations`
--
ALTER TABLE `Reservations`
  ADD CONSTRAINT `Reservations_ibfk_1` FOREIGN KEY (`UserId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
