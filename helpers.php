<?php
if (isset($_GET['logout'])) {
    unset($_COOKIE['user']);
    $cookieExpiry = time() - 60000;
    setcookie('user', '', $cookieExpiry, '/');
    setcookie('admin', '', $cookieExpiry, '/');
    session_destroy();
}
//$salt = 'r1e2s3t4';

function dd($what)
{
    echo "<pre>";
    print_r($what);
    echo "</pre>";
    die();
}