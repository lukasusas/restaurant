<?php

class AdminModel
{
    public function addNewProduct($name, $description, $quantity, $buyPrice, $salePrice)
    {
        $database = new Database();
        $photoName = time() . $_FILES['photoToUpload']['name'];

        $this->imageUpload($photoName);

        $sql = 'INSERT INTO `Meal` (Name, Description, Photo, QuantityInStock, BuyPrice, SalePrice) VALUES (?,?,?,?,?,?)';

        return $database->executeSql($sql, [
            $name,
            $description,
            $photoName,
            $quantity,
            $buyPrice,
            $salePrice
        ]);
    }

    public function updateProductInfo($name, $description, $quantity, $buyPrice, $salePrice)
    {
        $database = new Database();
        $newPhotoName = time() . $_FILES['photoToUpload']['name'];
        $this->imageUpload($newPhotoName);

        if (strlen($_FILES['photoToUpload']['name']) > 0) {
            $sql = 'UPDATE `Meal` SET Name="' . $name . '", Description="' . $description . '", Photo="' . $newPhotoName . '", QuantityInStock="' . $quantity . '", BuyPrice="' . $buyPrice . '", SalePrice="' . $salePrice . '" WHERE id="' . $_GET['edit'] . '"';
        } else {
            $sql = 'UPDATE `Meal` SET Name="' . $name . '", Description="' . $description . '", QuantityInStock="' . $quantity . '", BuyPrice="' . $buyPrice . '", SalePrice="' . $salePrice . '" WHERE id="' . $_GET['edit'] . '"';
        }

        return $database->query($sql);

    }

    public function deleteProduct()
    {
        $database = new Database();

        $sql = 'DELETE FROM `Meal` WHERE id="' . $_GET['delete'] . '"';

        return $database->query($sql);
    }

    public function listAllUsers()
    {
        $database = new Database();

        $sql = 'SELECT * FROM `Users`';

        return $database->query($sql);
    }

    public function deleteUserByID($userId)
    {
        $database = new Database();

        $sql = 'DELETE FROM `Users` WHERE id="' . $userId . '"';

        return $database->query($sql);
    }

    public function editUserByID($formData, $userID)
    {
        $profileModel = new ProfileModel();
        $userPass = $profileModel->userData($userID);
        $password = $userPass[0]['Password'];
        $database = new Database();
        if(strlen($formData['password']) < 1) {
            $formData['password'] = $password;
        } else {
            $formData['password'] = md5('r1e2s3t4' . $formData['password']);
        }

        $sql = 'UPDATE `Users` SET Name="' . $formData['name'] . '", Surname="' . $formData['surname'] . '", Email="' . $formData['email'] . '", PhoneNumber="' . $formData['phone'] . '", Password="' . $formData['password'] . '", Streetname="' . $formData['street'] . '", HouseNumber="' . $formData['house'] . '", City="' . $formData['city'] . '", Role="' . $formData['role'] . '" WHERE id="' . $userID . '"';

        return $database->query($sql);
    }

    private function imageUpload($photoName)
    {

        move_uploaded_file($_FILES['photoToUpload']['tmp_name'], WWW_PATH . '/images/meals/' . $photoName);
    }


}
