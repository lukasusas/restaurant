<?php

class HomeModel
{
    public function listAll()
    {
        $database = new Database();

        $sql = 'SELECT * FROM `Meal` ORDER BY SalePrice ASC';

        return $database->query($sql);
    }

    public function getOneProduct($id)
    {
        $database = new Database();

        $sql = 'SELECT * FROM `Meal` WHERE id="' . $id . '"';

        return $database->query($sql);
    }

    public function getPriceOfCart($cartArray)
    {
        $totalSum = 0;
        foreach ($cartArray as $mealInCart) {
            $productInfo = $this->getOneProduct($mealInCart['mealID']);
            $totalSum += $productInfo[0]['SalePrice']*$mealInCart['quantity'];
        }
        return $totalSum;
    }


}


?>