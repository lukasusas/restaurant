<?php

class ProfileModel
{
    public function userData($userID)
    {
        $database = new Database();

        $sql = 'SELECT * FROM `Users` WHERE id="' . $userID . '"';

        return $database->query($sql);
    }

    public function updateUser($name, $surname, $phoneNumber, $streetName, $houseNumber, $city, $userID)
    {
        $database = new Database();

        $sql = 'UPDATE `Users` SET Name="' . $name . '" , Surname="' . $surname . '" , PhoneNumber="' . $phoneNumber . '" , StreetName="' . $streetName . '" , HouseNumber="' . $houseNumber . '" , City="' . $city . '" WHERE id="' . $userID . '"';

        return $database->query($sql);
    }

    public function changePassword($pass, $userID)
    {
        $database = new Database();

        $sql = 'UPDATE `Users` SET Password="' . md5('r1e2s3t4' . $pass) . '" WHERE id="' . $userID . '"';

        return $database->query($sql);
    }

    public function takeIdFromCookie()
    {
        $userID = openssl_decrypt($_COOKIE['user'], 'aes-256-cbc', 'restaurant', '1', '1234567887654321');
        return $userID;
    }


}
