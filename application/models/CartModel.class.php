<?php

class CartModel
{
    public function listAll()
    {
        $database = new Database();

        $sql = 'SELECT * FROM `Meal` ORDER BY SalePrice DESC';

        return $database->query($sql);
    }

    public function createOrder()
    {

        $database = new Database();
        $profileModel = new ProfileModel();
        $userID = $profileModel->takeIdFromCookie();
        $totalSum = $_SESSION['totalSum'];
        $taxRate = 0.21;
        $taxAmount = ($totalSum-$totalSum/1.21);


        $orderSql = 'INSERT INTO `Orders` (UserId, TotalAmount, TaxRate, TaxAmount) VALUES (?, ?, ?, ?)';

        $database->executeSql($orderSql, [
            $userID,
            $totalSum,
            $taxRate,
            $taxAmount
        ]);
        foreach ($_SESSION['cart'] as $orderline) {
            $orderlineSql = 'INSERT INTO `Orderline` (QuantityOrdered, MealId, OrderId, PriceEach) VALUES (?,?,(SELECT max(id) FROM `Orders`),(SELECT BuyPrice FROM `Meal` WHERE id="'. $orderline['mealID'] .'"))';

            $database->executeSql($orderlineSql,[
                $orderline['quantity'],
                $orderline['mealID']
            ]);
        }
        foreach ($_SESSION['cart'] as $orderline) {
            $quantityAvailableSql = 'SELECT * FROM `Meal` WHERE id="' . $orderline['mealID'] . '"';
            $quantityAvailable = $database->query($quantityAvailableSql);

            $newQuantity = $quantityAvailable[0]['QuantityInStock']-$orderline['quantity'];
            $quantityAvailableEditSql = 'UPDATE `Meal` SET QuantityInStock="' . $newQuantity . '" WHERE id="' . $orderline['mealID'] . '"';

            $database->query($quantityAvailableEditSql);

        }

    }


}
