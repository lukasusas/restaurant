<?php

class ReservationsModel
{
    public function addReservation($name, $persons, $date, $time, $phone, $userID = '')
    {

        $database = new Database();
        if (isset($_COOKIE['user'])) {

            $sql = 'INSERT INTO `Reservations` (Name, NumberOfPersons, Date, Time, PhoneNumber, UserId) VALUES ("' . $name . '", "' . $persons . '", "' . $date . '", "' . $time . '", "' . $phone . '", "' . $userID . '")';

        } else {
            $miniUserId = $this->getMiniUserId($name, $phone);

            $sql = 'INSERT INTO `Reservations` (Name, NumberOfPersons, Date, Time, PhoneNumber, UserId) VALUES ("' . $name . '", "' . $persons . '", "' . $date . '", "' . $time . '", "' . $phone . '", "' . $miniUserId . '")';

            $this->loginMiniUser($miniUserId);
        }

        return $database->query($sql);
    }

    public function registerMiniUser($name, $phone)
    {
        $database = new Database();

        $sql = 'INSERT INTO `Users` (Name, PhoneNumber, Role) VALUES ("' . $name . '", "' . $phone . '", "2")';

        return $database->query($sql);

    }

    private function getMiniUserId($name, $phone)
    {
        $database = new Database();

        $sql = 'SELECT `id` FROM `Users` WHERE Name="' . $name . '" AND PhoneNumber="' . $phone . '"';

        $miniId = $database->query($sql);

        return $miniId[0]['id'];
    }

    private function loginMiniUser($miniUserId)
    {
        $cookieExpiry = time()+60*60*24*100;
        $cookieString = openssl_encrypt($miniUserId, 'aes-256-cbc', 'restaurant', '1', '1234567887654321');
        setcookie('user', $cookieString , $cookieExpiry, '/');
    }


}


?>