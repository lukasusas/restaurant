<?php


class CartController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {
        $homeModel = new HomeModel();
        $shoppingCart = [];
        foreach ($_SESSION['cart'] as $mealInCart) {
            $mealInfo = $homeModel->getOneProduct($mealInCart['mealID']);
            $mealInfo[0]['quantity'] = $mealInCart['quantity'];
            array_push($shoppingCart, $mealInfo[0]);
        }

        return [
            'meals' => $shoppingCart,
        ];
    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $homeModel = new HomeModel();
        $editID = $queryFields['mealID'];
        $thisItemQuantity = 0;
        if (isset($queryFields['deleteID'])) {
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['mealID'] == $editID) {
                    unset($_SESSION['cart'][$i]);
                    unset($_SESSION['cartDropdown'][$i]);

                }
            }
        } else {
            $editQuantity = $queryFields['quantity'];
            $oneMealInfo = $homeModel->getOneProduct($editID);
            $mealQuantityAvailable = $oneMealInfo[0]['QuantityInStock'];
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['mealID'] == $editID) {
                    if ($editQuantity == 0 || isset($_POST['deleteID'])) {
                        unset($_SESSION['cart'][$i]);
                    } else {
                        if ($editQuantity >= $mealQuantityAvailable) {
                            $_SESSION['cart'][$i]['quantity'] = $mealQuantityAvailable;
                            $thisItemQuantity = $_SESSION['cart'][$i]['quantity'];
                        } else {
                            $_SESSION['cart'][$i]['quantity'] = $editQuantity;
                            $thisItemQuantity = $_SESSION['cart'][$i]['quantity'];
                        }
                    }
                }
                if ($_SESSION['cartDropdown'][$i]['id'] == $editID) {
                    if ($editQuantity == 0 || isset($_POST['deleteID'])) {
                        unset($_SESSION['cartDropdown'][$i]);
                    } else {
                        if ($editQuantity >= $mealQuantityAvailable) {
                            $_SESSION['cartDropdown'][$i]['quantity'] = $mealQuantityAvailable;
                        } else {
                            $_SESSION['cartDropdown'][$i]['quantity'] = $editQuantity;
                        }
                    }
                }
            }
        }
        $_SESSION['cart'] = array_values($_SESSION['cart']);
        $_SESSION['cartDropdown'] = array_values($_SESSION['cartDropdown']);
        $_SESSION['totalSum'] = $homeModel->getPriceOfCart($_SESSION['cart']);
//        $http->redirectTo('/cart');
        $http->sendJsonResponse([
            "cartDropdown" => $_SESSION['cartDropdown'],
            "totalSum" => $_SESSION['totalSum'],
            "cart" => $_SESSION['cart'],
            "thisItemQuantity" => $thisItemQuantity
        ]);
    }

}
