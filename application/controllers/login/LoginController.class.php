<?php


class LoginController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $loginModel = new LoginModel();
        $userData = $loginModel->loginCheck($queryFields['email'], $queryFields['password']);
        if (count($userData) > 0) {
            $loginModel->loginSuccessful($userData[0]['id']);
            $cookieExpiry = time()+60*60*24*100;
            $cookieString = openssl_encrypt($userData[0]['id'], 'aes-256-cbc', 'restaurant', '1', '1234567887654321');
//            dd(openssl_decrypt($cookieString, 'aes-256-cbc', 'restaurant', '1', '1234567887654321'));
            setcookie('user', $cookieString , $cookieExpiry, '/');
            if ($userData[0]['Role'] == 3) {
                setcookie('admin', 'superSecretPhrase', $cookieExpiry, '/');
            }
            $http->redirectTo('/');
            exit();

        }
    }

}
