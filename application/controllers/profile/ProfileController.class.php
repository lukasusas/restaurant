<?php


class ProfileController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {
        return [
            'queryData' => $this->getUserProfile()
        ];

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $_SESSION['data'] = $queryFields;
        $_SESSION['errors'] = [];

        $updateUser = new ProfileModel();
        $userID = $updateUser->takeIdFromCookie();
        $this->checkForErrors($queryFields);
        if (count($_SESSION['errors']) == 0) {
            if (isset($queryFields['password'])) {
                $updateUser->changePassword($queryFields['password'], $userID);
                setcookie('successprofile', 'You have successfully changed password!', time()+5, '/');
            } else {

                $updateUser->updateUser($queryFields['name'], $queryFields['surname'], $queryFields['phone'], $queryFields['street'], $queryFields['house'], $queryFields['city'], $userID);
                setcookie('successprofile', 'You have successfully updated info!', time()+5, '/');
            }
            unset($_SESSION['data']);
            unset($_SESSION['errors']);
            $http->redirectTo('/profile');
        }
        return [
            'queryData' => $this->getUserProfile()
        ];
    }

    public function takeIdFromCookie()
    {
        $userID = openssl_decrypt($_COOKIE['user'], 'aes-256-cbc', 'restaurant', '1', '1234567887654321');
        return $userID;
    }

    public function getUserProfile()
    {
        $userData = new ProfileModel();
        $userID = $this->takeIdFromCookie();
        $queryData = $userData->userData($userID);

        return $queryData;
    }

    private function checkForErrors($checkWhat)
    {
        if (isset($checkWhat['name'])) {
            if (!is_string($checkWhat['name'])) {
                array_push($_SESSION['errors'], 'Incorrect name input');
            }
            if (!is_string($checkWhat['surname'])) {
                array_push($_SESSION['errors'], 'Incorrect surname input');
            }
            if (!is_string($checkWhat['phone']) || strlen($checkWhat['phone']) < 6 || strlen($checkWhat['phone']) > 16) {
                array_push($_SESSION['errors'], 'Incorrect phone number, must be between 6 and 16 characters long');
            }
            if (!is_string($checkWhat['street'])) {
                array_push($_SESSION['errors'], 'Incorrect street address');
            }
            if (!is_string($checkWhat['house'])) {
                array_push($_SESSION['errors'], 'Incorrect street address');
            }
            if (!is_string($checkWhat['city'])) {
                array_push($_SESSION['errors'], 'Incorrect city address');
            }
        }
        if (isset($checkWhat['password'])) {
            $userPassFromDB = $this->getUserProfile()[0]['Password'];
            $salt = 'r1e2s3t4';
            $oldPass = md5($salt . $checkWhat['oldPassword']);
            if ($oldPass != $userPassFromDB) {
                array_push($_SESSION['errors'], 'Old password is incorrect');
            } else {
                if ($checkWhat['password'] != $checkWhat['passRepeat']) {
                    array_push($_SESSION['errors'], 'New passwords do not match');
                }
                elseif (strlen($checkWhat['password']) < 6){
                    array_push($_SESSION['errors'], 'New password too short');
                }
            }
        }
    }


}
