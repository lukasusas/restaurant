<?php


class RegisterController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $_SESSION['data'] = $queryFields;
        $_SESSION['errors'] = [];
        $registerModel = new RegisterModel();
        $emailList = $registerModel->getEmails($queryFields['email']);
        $this->checkForIllegalChars($queryFields);
        if (count($emailList) != 0) {
            array_push($_SESSION['errors'], 'This email already exists');
        }
        if (count($_SESSION['errors']) == 0) {
            $registerModel->registerUser(strip_tags($queryFields['name']), strip_tags($queryFields['surname']), strip_tags($queryFields['email']), strip_tags($queryFields['phone']), strip_tags($queryFields['password']), strip_tags($queryFields['street']), strip_tags($queryFields['house']), strip_tags($queryFields['city']));
            setcookie('successregister', '1', time()+5, '/');
            unset($_SESSION['data']);
            unset($_SESSION['errors']);
            $http->redirectTo('/register');
        }

    }

    public function checkForIllegalChars($checkWhat)
    {
        if (is_string($checkWhat['name'])) {
            if (is_string($checkWhat['surname'])) {
                if (!filter_var($checkWhat['email'], FILTER_VALIDATE_EMAIL) === false) {
                    if (is_string($checkWhat['phone']) && strlen($checkWhat['phone']) > 6 && strlen($checkWhat['phone']) < 16) {
                        if (is_string($checkWhat['street'])) {
                            if (is_string($checkWhat['house'])) {
                                if (is_string($checkWhat['city'])) {
                                    if (is_string($checkWhat['password']) && $checkWhat['password'] == $checkWhat['passRepeat'] && strlen($checkWhat['password']) >= 6) {
                                        return true;
                                    } else {
                                        array_push($_SESSION['errors'], 'Passwords do not match or too short(min 6 symbols)');
                                    }
                                } else {
                                    array_push($_SESSION['errors'], 'Incorrect city input');
                                }
                            } else {
                                array_push($_SESSION['errors'], 'Incorrect house number');
                            }
                        } else {
                            array_push($_SESSION['errors'], 'Incorrect street name');
                        }
                    } else {
                        array_push($_SESSION['errors'], 'Incorrect phone number');
                    }

                } else {
                    array_push($_SESSION['errors'], 'Incorrect email address');
                }
            } else {
                array_push($_SESSION['errors'], 'Incorrect surname input');
            }
        } else {
            array_push($_SESSION['errors'], 'Incorrect name input');
        }
    }


}

?>