<?php

require_once getcwd() . '/application/controllers/profile/ProfileController.class.php';

class ReservationsController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {
        if (isset($_COOKIE['user'])) {
            $userData = new ProfileController();
            return [
                'userID' => $userData->takeIdFromCookie(),
                'userInfo' => $userData->getUserProfile()
            ];
        }
    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $_SESSION['data'] = $queryFields;
        $_SESSION['errors'] = [];
        $reservationsModel = new ReservationsModel();
        $checker = $this->checkForIllegalChars($queryFields);
        foreach ($queryFields as $field){
            $field = strip_tags($field);
        }
        if ($checker == true) {
            if (isset($_COOKIE['user'])) {
                $cookieID = new ProfileController();
                $userID = $cookieID->takeIdFromCookie();
                $reservationsModel->addReservation($queryFields['name'], $queryFields['persons'], $queryFields['date'], $queryFields['time'], $queryFields['phone'], $userID);
            } else {
                $reservationsModel->registerMiniUser($queryFields['name'], $queryFields['phone']);
                $reservationsModel->addReservation($queryFields['name'], $queryFields['persons'], $queryFields['date'], $queryFields['time'], $queryFields['phone']);

            }
            setcookie('success', '1', time()+5, '/');
            unset($_SESSION['data']);
            unset($_SESSION['errors']);
            $http->redirectTo('/reservations');
        }

    }

    public function checkForIllegalChars($checkWhat)
    {
        if (is_string($checkWhat['name'])) {
            if (is_int((int)$checkWhat['persons']) && (int)$checkWhat['persons'] < 20 && (int)$checkWhat['persons'] > 0) {
                if ($this->validateDate($checkWhat['date'], 'Y-m-d') == true) {
                    if ($this->validateDate($checkWhat['time'], 'H:i') == true) {
                        if (is_string($checkWhat['phone']) && strlen($checkWhat['phone']) > 6 && strlen($checkWhat['phone']) < 16) {
                            return true;
                        } else {
                            array_push($_SESSION['errors'], 'Incorrect phone number');
                        }

                    } else {
                        array_push($_SESSION['errors'], 'Incorrect time format');
                    }

                } else {
                    array_push($_SESSION['errors'], 'Incorrect date format');
                }

            } else {
                array_push($_SESSION['errors'], 'Incorrect number of persons');
            }
        } else {
            array_push($_SESSION['errors'], 'Incorrect format of name');
        }


    }

    function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}
