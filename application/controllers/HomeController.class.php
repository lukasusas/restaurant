<?php

class HomeController
{
    public function httpGetMethod(Http $http, array $queryFields)
    {
        if(isset($_GET['logout'])){
            $http->redirectTo('/');
        }
        if (!isset($_SESSION['cart'])) {
            $_SESSION['cart'] = [];
        }
        $homeModel = new HomeModel();


        $meals = $homeModel->listAll();


        if (isset($_GET['mealID'])) {
            $mealID = $queryFields['mealID'];
            $quantity = $queryFields['quantity'];
            $oneMealInfo = $homeModel->getOneProduct($mealID);
            $mealQuantityAvailable = $oneMealInfo[0]['QuantityInStock'];

            $checkIfExists = -1;
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['mealID'] == $mealID) {
                    if ($_SESSION['cart'][$i]['quantity'] + $quantity >= $mealQuantityAvailable) {
                        $_SESSION['cart'][$i]['quantity'] = $mealQuantityAvailable;
                    } else {
                        $_SESSION['cart'][$i]['quantity'] += $quantity;
                    }
                    $checkIfExists = 1;
                }
            }
            if ($checkIfExists == -1) {
                if($quantity > $mealQuantityAvailable){
                    $quantity = $mealQuantityAvailable;
                }
                array_push($_SESSION['cart'], ['quantity' => $quantity, 'mealID' => $mealID]);
            }
//        for cart dropdown in layoutview
            $shoppingCart = [];
            foreach ($_SESSION['cart'] as $mealInCart) {
                $mealInfo = $homeModel->getOneProduct($mealInCart['mealID']);
                $mealInfo[0]['quantity'] = $mealInCart['quantity'];
                array_push($shoppingCart, $mealInfo[0]);
            }
            $_SESSION['cartDropdown'] = $shoppingCart;
//        end for cart dropdown

        }
        if (!isset($_SESSION['totalSum'])) {
            $_SESSION['totalSum'] = 0;
        }
        $_SESSION['totalSum'] = $homeModel->getPriceOfCart($_SESSION['cart']);

        if (isset($_SESSION['cart'])) {
            return [
                'meals' => $meals,
                'totalSum' => $_SESSION['totalSum'],
            ];

        } else {

            return [
                'meals' => $meals,
            ];
        }

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {

        $homeModel = new HomeModel();

        if (isset($queryFields['mealID'])) {
            $mealID = $queryFields['mealID'];
            $quantity = $queryFields['quantity'];
            $oneMealInfo = $homeModel->getOneProduct($mealID);
            $mealQuantityAvailable = $oneMealInfo[0]['QuantityInStock'];

            $checkIfExists = -1;
            for ($i = 0; $i < count($_SESSION['cart']); $i++) {
                if ($_SESSION['cart'][$i]['mealID'] == $mealID) {
                    if ($_SESSION['cart'][$i]['quantity'] + $quantity >= $mealQuantityAvailable) {
                        $_SESSION['cart'][$i]['quantity'] = $mealQuantityAvailable;
                    } else {
                        $_SESSION['cart'][$i]['quantity'] += $quantity;
                    }
                    $checkIfExists = 1;
                }
            }
            if ($checkIfExists == -1) {
                if($quantity > $mealQuantityAvailable){
                    $quantity = $mealQuantityAvailable;
                }
                array_push($_SESSION['cart'], [
                    'quantity' => $quantity,
                    'mealID' => $mealID
                ]);
            }
//        for cart dropdown in layoutview
            $shoppingCart = [];
            foreach ($_SESSION['cart'] as $mealInCart) {
                $mealInfo = $homeModel->getOneProduct($mealInCart['mealID']);
                $mealInfo[0]['quantity'] = $mealInCart['quantity'];
                array_push($shoppingCart, $mealInfo[0]);
            }
            $_SESSION['cartDropdown'] = $shoppingCart;
//        end for cart dropdown

        }
        if (!isset($_SESSION['totalSum'])) {
            $_SESSION['totalSum'] = 0;
        }
        $_SESSION['totalSum'] = $homeModel->getPriceOfCart($_SESSION['cart']);

        $http->sendJsonResponse([
            "totalSum" => $_SESSION['totalSum'],
            "cartDropdown" => $_SESSION['cartDropdown']
        ]);
    }


}