<?php


class NewproductController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        foreach ($queryFields as $field) {
            $field = strip_tags($field);
        }

        $model = new AdminModel();
        $model->addNewProduct($queryFields['name'], $queryFields['description'], $queryFields['stockQuantity'], $queryFields['buyPrice'], $queryFields['salePrice']);

    }

}
