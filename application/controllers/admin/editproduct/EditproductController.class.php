<?php


class EditproductController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {

        $homeModel = new HomeModel();
        $meals = $homeModel->listAll();
        if(isset($_GET['delete'])){
            $adminModel = new AdminModel();
            $adminModel->deleteProduct();
            $http->redirectTo("/admin/editproduct");
        }
        if (isset($_GET['edit'])) {
            $oneMeal = $homeModel->getOneProduct($_GET['edit']);
            return [
                'meals' => $meals,
                'oneMeal' => $oneMeal,
            ];
        } else {
            return [
                'meals' => $meals,
            ];
        }

    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        foreach ($queryFields as $field) {
            $field = strip_tags($field);
        }

        $model = new AdminModel();
        $model->updateProductInfo($queryFields['name'], $queryFields['description'],                    $queryFields['stockQuantity'], $queryFields['buyPrice'], $queryFields['salePrice']);

        $http->redirectTo("/admin/editproduct");
    }

}
