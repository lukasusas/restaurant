<?php


class EditusersController
{

    public function httpGetMethod(Http $http, array $queryFields)
    {
        $userModel = new AdminModel();
        $userInfoArray = [];
        if (isset($_GET['deleteUserID'])) {
            $userModel->deleteUserByID($_GET['deleteUserID']);
        }
        if (isset($_GET['editUserID'])) {
            $profileModel = new ProfileModel();
            $userInfoArray = $profileModel->userData($_GET['editUserID']);
            $userInfoArray = $userInfoArray[0];
        }

        return [
            'users' => $userModel->listAllUsers(),
            'userInfo' => $userInfoArray,
        ];
    }

    public function httpPostMethod(Http $http, array $queryFields)
    {
        $adminModel = new AdminModel();
        foreach ($queryFields as $field) {
            $field = strip_tags($field);
        }

        $adminModel->editUserByID($queryFields, $_GET['editUserID']);

        $http->redirectTo('/admin/editusers');
    }

}
