/////////////////////////////////////////////////////////////////////////////////////////
// FUNCTIONS                                                                           //
/////////////////////////////////////////////////////////////////////////////////////////

function rebuildCartDropdown(cart) {
    var list = $('.dropdown-cart-items');

    list.empty();

    for (i = 0; i < cart.length; i++) {
        list.append('<li> <span class="item"> <span class="item-left"> <img src="/Restaurant/application/www/images/meals/' + cart[i].Photo + '" alt=""/> <span class="item-info"> <span>' + cart[i].Name + '</span> <span>' + cart[i].quantity + 'X</span> </span> </span> <span class="item-right"> <span>€' + cart[i].SalePrice * cart[i].quantity + '</span> </span> </span> </li>');
    }
}

function rebuildTotalSum(totalSum) {
    var totalSumLink = $('.cart-total-sum');
    totalSumLink.empty();
    totalSumLink.append('Cart €' + totalSum);
}

function rebuildTotalSumInCartView(totalSum) {
    var totalSumCarView = $('#cart-view-total-sum');
    var totalSumCartViewMobile = $('.totalSum-mobile-cart');
    totalSumCarView.empty();
    totalSumCartViewMobile.empty();
    totalSumCarView.append('€' + totalSum);
    totalSumCartViewMobile.append('€' + totalSum);
}

function rebuildCartViewSums(mealId, quantity, cartDropdown) {
    var tr = $('#mealID' + mealId).find('.price-amount');
    for (var i = 0; i < cartDropdown.length; i++) {
        if (cartDropdown[i].id == mealId) {
            var price = parseFloat(cartDropdown[i].SalePrice);
        }
    }
    quantity = parseInt(quantity);
    tr.empty();
    tr.append('€' + (quantity * price));
}
function removeFromCartViewOnDelete(mealId) {
    var tr = $('#mealID' + mealId);
    tr.remove();
}
function rebuildQuantityInputValue(mealId, quantity) {
    var input = $('table').find('input[data-meal-id=' + mealId + ']');
    input.val(quantity);

}

/////////////////////////////////////////////////////////////////////////////////////////
// CODE                                                                      //
/////////////////////////////////////////////////////////////////////////////////////////
$(document).ready(function () {
    // for large devices
    if ($(window).width() > 1024) {
        $('footer').addClass('navbar-fixed-bottom');

    }

    if ($(window).width() > 767) {
// ADD SLIDEUP ANIMATION TO DROPDOWN //
        $('.dropdown').on('hide.bs.dropdown', function (e) {
            $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
        });
// ADD SLIDEDOWN ANIMATION TO DROPDOWN //
        $('.dropdown').on('show.bs.dropdown', function (e) {
            $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
        });
    }
    //end for large devices

    $('#date').datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: 0,
        showAnim: 'slideDown'
    });
    $('#time').timepicker({
        stepMinute: 15,
        hourMin: 10,
        hourMax: 22,
        showAnim: 'slideDown'
    });

    $('.space-filling-image').mouseover(function () {
        $(this).css({
            opacity: '1',
            transition: 'all 1s'
        });
    });
    $('.space-filling-image').mouseleave(function () {
        $(this).css({
            opacity: '0.3',
            transition: 'all 2s'
        });
    });

//                   AJAX

    $('.add-to-cart').click(function (e) {
        e.preventDefault();

        var mealId = $(this).data('meal-id');
        var quantity = $(this).parent().parent().find('input[name="quantity"]').val();

        if (quantity) {
            $.ajax({
                method: 'post',
                url: '',
                data: {
                    mealID: mealId,
                    quantity: quantity
                },
                dataType: 'json',
                success: function (response) {
                    rebuildCartDropdown(response.cartDropdown);
                    rebuildTotalSum(response.totalSum);
                }
            })
        }

    });

    $('.edit-cart').change(function (e) {
        e.preventDefault();
        var mealId = $(this).data('meal-id');
        var quantity = $(this).val();

        if (quantity) {
            $.ajax({
                method: 'post',
                url: 'cart',
                data: {
                    mealID: mealId,
                    quantity: quantity
                },
                dataType: 'json',
                success: function (response) {
                    rebuildQuantityInputValue(mealId, response.thisItemQuantity);
                    rebuildCartDropdown(response.cartDropdown);
                    rebuildTotalSum(response.totalSum);
                    rebuildTotalSumInCartView(response.totalSum);
                    rebuildCartViewSums(mealId, response.thisItemQuantity, response.cartDropdown);
                }
            })
        }
    });

    $('.delete-from-cart').on('click', function (e) {
        e.preventDefault();
        var mealId = $(this).data('meal-id');

        $.ajax({
            method: 'post',
            url: 'cart',
            data: {
                mealID: mealId,
                deleteID: 'delete'
            },
            dataType: 'json',
            success: function (response) {
                removeFromCartViewOnDelete(mealId);
                rebuildCartDropdown(response.cartDropdown);
                rebuildTotalSum(response.totalSum);
                rebuildTotalSumInCartView(response.totalSum);
            }
        })

    });

}); // document ready end


